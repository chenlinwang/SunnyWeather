package com.sunnyweather.android.util

import android.content.Context
import android.widget.Toast
import com.sunnyweather.android.SunnyWeatherApplication

object ToastUtil {

    //类似于静态方法,里面的方法可以被直接调用
    fun toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(SunnyWeatherApplication.context, message, duration).show()
    }
}